%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1

%define NEWLINE_SYMBOL 0xA
%define TAB_SYMBOL 0x9
%define SPACE_SYMBOL 0x20

%define STDOUT 1
%define STDIN 0

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count:
      cmp byte [rdi+rax], 0
      je .end
      inc rax
      jmp .count
    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, WRITE_SYSCALL
    mov rdi, 1
    mov rsi, rsp 
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_SYMBOL
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, rsp
    dec rsp
    mov byte[rsp], 0x0 
    mov r9, 10
    .loop:
        xor rdx, rdx
        div r9
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        test rax, rax
        jz .print
        jmp .loop
    .print:
        mov rdi, rsp
        call print_string
        mov rsp, r8
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

    .print:
        call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    cmp rdi, rsi
    je .equal
    mov r8, rsi
    mov r9, rdi
    call string_length
    mov r10, rax
    mov rdi, r8
    call string_length
    mov rsi, r8
    mov rdi, r9
    cmp r10, rax
    jne .not_equal

    .equal_char:
    mov r8b, [rdi]
    mov r9b, [rsi]
    cmp r9b, r8b
    jne .not_equal
    test r9b, r8b
    jz .equal
    inc rdi
    inc rsi
    jmp .equal_char

    .equal:
        mov rax, 1
        ret
    .not_equal:
        xor rax, rax
        ret
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0 ;read syscall
    xor rdi, rdi ;stdin
    mov rdx, 1
    push rax
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8

    .loop:
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi

    test rax, rax
    jz .end

    .gap_check:
    cmp rax, TAB_SYMBOL
    je .check_end_of_word
    cmp rax, SPACE_SYMBOL
    je .check_end_of_word
    cmp rax, NEWLINE_SYMBOL
    je .check_end_of_word


    mov byte[rdi+r8], al
    inc r8
    cmp r8, rsi
    jg .overflow_end
    jmp .loop

    .check_end_of_word:
    test r8, r8
    jz .loop
    jmp .end

    .overflow_end:
    xor rax, rax
    ret

    .end:
    mov byte[rdi+r8], 0x0
    mov rdx, r8
    mov rax, rdi
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    xor r8, r8
    mov r9b, 0x30 ; число 0
    mov r10b, 0x39 ; число 9
    mov r11, 10

    .loop:
    mov r8b, byte[rdi+rcx]
    cmp r8b, 0x30
    jl .exit
    cmp r8b, r10b
    jg .exit
    mul r11
    sub r8, 0x30 ; число 0
    add rax, r8
    inc rcx
    jmp .loop

    .exit:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    mov r8b, byte[rdi]

    cmp r8b, '-' ;
    je .minus

    cmp r8b, '+' ;
    je .plus

    call parse_uint
    ret

    .minus:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
    
    .plus:
    inc rdi
    call parse_uint
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r8, r8
    mov [rsi+rcx], r8
    call string_length

    cmp rdx, rax
    jb .overflow_end
    xor rcx, rcx

    .loop:
    cmp rcx, rax
    je .end
    mov r8, [rdi+rcx]
    mov [rsi + rcx], r8
    inc rcx
    jmp .loop

    .overflow_end:
    xor rax, rax
    ret

    .end:
    ret
